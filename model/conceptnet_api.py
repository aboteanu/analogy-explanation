import urllib, urllib2, json
import string
import json
import time
import traceback
from model.conceptnet_static import NWORD_CONCEPTS, API_URL, CN_RESULT_LIMIT
from model.nlp import useful_word
from model.caches.everything_per_task import EverythingPerTaskCache

# need to source virtualenv first
from conceptnet5.query import lookup
LOCAL = True

cache = EverythingPerTaskCache()

def clear_cache():
	cache.endOfTaskCleanup()

def conceptnet_api( input_name ):
	assert type(input_name) == str or type(input_name) == unicode, input_name
	# replace spaces with _ e.g. "dinner plate" -> "dinner_plate"
	name = '_'.join( input_name.split(' ') )

	if name in cache:
		data = cache[name]
		return data
	elif LOCAL:
		data = list() 
		k=0
		for edge in conceptnet_api_local( name ):
			data.append( edge )
			k += 1
			if k >= CN_RESULT_LIMIT:
				break
		if len( data ) > 0:
			cache[name] = data
			return data
		else:
			return list()
	else:
		json_iter = conceptnet_api_web(name)
		json_data = list(json_iter)
		if len( json_data ) > 0:
			cache[name] = json_data
			return json_data
		else:
			return list()

def conceptnet_api_local( name ):
	return lookup( '/c/en/'+name )

def conceptnet_api_web( name ):
	'''
	Go through all pages and read all results for this name
	The API caps the results at 1000, page size is 50 by default

	>>> assert len(list(conceptnet_api('dog'))) == CN_RESULT_LIMIT
	>>> len(list(conceptnet_api('gfrejgierogjo')))
	1
	'''
	offset = 0
	cursor = 0
	num_found = -1
	nread = 0

	concept_url = build_concept_url( name, offset ) 
	conn = urllib2.urlopen( concept_url )
	response = conn.read()
	concept_json = json.loads( response )

	num_found = concept_json.get('numFound')
	edges = concept_json.get('edges')
	window_size = len(concept_json['edges'])

	if num_found==0:
		yield None

	while nread<num_found:
		if cursor < len(edges):
			yield edges[cursor]
			cursor+=1
			nread+=1

		else:
			offset+=window_size
			cursor=0

			concept_url = build_concept_url( name, offset ) 
			conn = urllib2.urlopen( concept_url )
			response = conn.read()
			concept_json = json.loads( response )

			assert num_found == concept_json.get('numFound')
			edges = concept_json.get('edges')
			window_size = len(concept_json['edges'])
			# will loop again and yield on the other if branch

def conceptnet_edges_between( c1, c2 ):
	'''
	Return all edges between two concepts
	as a list of (rel type, weight) tuples
	'''
	result = list()
	json1 = conceptnet_api( c1 )

	for edge in json1:
		startlemma = uri_lemma( edge.get('start') )
		endlemma = uri_lemma( edge.get('end') )
		if not startlemma or not endlemma:
			continue
		startlemma = startlemma.replace('_', ' ')
		endlemma = endlemma.replace('_', ' ')
		if c2 == startlemma or c2 == endlemma:
			result.append( (edge.get(u'rel'), edge.get(u'weight') ) )

	return result

def build_concept_url( name, offset ):
	try:
		url = API_URL+name+'?limit='+str(CN_RESULT_LIMIT)+'&offset='+str(offset)
		return url
	except Exception, err:
		print name
		traceback.print_exc(err)

def related_concepts(c, rj):
	assert rj!=None

	#iterate through the edges, each has an end and start lemma
	related = set()
	for x in rj:
		if not x:
			return related
		try:
			#skip translations
			rel = x.get('rel')
			if rel is None:
				continue
			if rel==u'/r/TranslationOf':
				continue

			if 'end' in x:
				l = x.pop('end')
				l = l.split('/')[-1]
				if l!=c and useful_word(l):
					related.add((l,rel,0))
			if 'start' in x:
				l = x.pop('start')
				l = l.split('/')[-1]
				if l!=c and useful_word(l):
					related.add((l,rel,1))
		except UnicodeEncodeError:
		#don't handle chars from other languages
			continue
	return related

def node_expansion(x):
	'''
	return a set of all neighbors of concept x
	'''
	neighbors = set()
	rjx = conceptnet_api(x)
	if rjx is not None:
		rcx = related_concepts(x, rjx)
		for yy, rel, direction in rcx:
			neighbors.add(yy)
	return neighbors

def node_degree(x):
	'''
	number of isA edges connecting the node in cn
	'''
	xjson = concept_json(x)
	degree=0
	for x in iter(xjson['edges']):
		if 'rel' in x:
			rel = x.pop('rel')
			if rel=='/r/IsA':
				degree+=1
	return degree

def uri_lemma( uri ):
	'''
	Get the lemma from a uri
	'''
	if uri is None:
		return None
	tok = uri.split('/')
	if 'en' not in tok or tok[-1] == 'en':
		return None

	return tok[ 1 + tok.index('en') ]

if __name__=='__main__':
	import doctest
	doctest.testmod()
