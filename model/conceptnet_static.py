API_URL = 'http://conceptnet5.media.mit.edu/data/5.4/c/en/'
IGNORE_RELATEDTO = False
MAX_CONCEPTS = 50 
CN_RESULT_LIMIT=50
NWORD_CONCEPTS=1
conceptnet_all_relation_types = [u'/r/DerivedFrom', u'/r/IsA', u'/r/ConceptuallyRelatedTo', u'/r/RelatedTo', u'/r/InstanceOf', u'/r/PartOf', u'/r/SimilarTo', u'/r/MemberOf', u'/r/HasContext', u'/r/Synonym', u'/r/HasProperty', u'/r/Antonym', u'/r/ReceivesAction', u'/r/Causes', u'/r/UsedFor', u'/r/HasPrerequisite', u'/r/wordnet/adjectivePertainsTo', u'/r/MadeOf', u'/r/Entails', u'/r/AtLocation', u'/r/Attribute', u'/r/Desires', u'/r/NotIsA', u'/r/CapableOf', u'/r/HasA', u'/r/SymbolOf', u'/r/CausesDesire', u'/r/NotDesires', u'/r/MotivatedByGoal', u'/r/CreatedBy', u'/r/DefinedAs', u'/r/HasSubevent', u'/r/HasFirstSubevent', u'/r/HasLastSubevent', u'/r/SimilarSize', u'/r/LocatedNear', u'/r/wordnet/adverbPertainsTo', u'/r/NotHasProperty', u'/r/NotCapableOf', u'/r/Derivative', u'/r/InheritsFrom', u'/r/wordnet/participleOf', u'/r/NotHasA', u'/r/HasPainIntensity', u'/r/NotMadeOf', u'/r/HasPainCharacter']

crowdsourced_all_relation_types = [
		'is a', 
		'part of',  
		'synonym',  
		'antonym',  
		'is capable of',  
		'is at location',
		'has property',
		'is made of',
		'is used for',
		'causes',
		'increases',
		'greater',
		]

conceptnet_inheritable_relation_types = [ u'/r/IsA', u'/r/Entails' ]
conceptnet_unspecific_relation_types = [ u'/r/RelatedTo', u'/r/ConceptuallyRelatedTo' ]

conceptnet_all_relation_types.sort()

conceptnet_relation_english_names = dict()
conceptnet_relation_english_names[u'/r/DerivedFrom'] = "is derived from"
conceptnet_relation_english_names[ u'/r/IsA'] = "is a"
conceptnet_relation_english_names[ u'/r/ConceptuallyRelatedTo'] = "is conceptually related to"
conceptnet_relation_english_names[ u'/r/RelatedTo'] = "is related to"
conceptnet_relation_english_names[ u'/r/InstanceOf'] = "is an instance of"
conceptnet_relation_english_names[ u'/r/PartOf'] = "is a part of"
conceptnet_relation_english_names[ u'/r/SimilarTo'] = "is similar to"
conceptnet_relation_english_names[ u'/r/MemberOf'] = "is a member of"
conceptnet_relation_english_names[ u'/r/HasContext'] = "has context"
conceptnet_relation_english_names[ u'/r/Synonym'] = "is synonym to"
conceptnet_relation_english_names[ u'/r/HasProperty'] = "has property of"
conceptnet_relation_english_names[ u'/r/Antonym'] = "is antonym to"
conceptnet_relation_english_names[ u'/r/ReceivesAction'] = "receives action"
conceptnet_relation_english_names[ u'/r/Causes'] = "causes"
conceptnet_relation_english_names[ u'/r/UsedFor'] = "is used for"
conceptnet_relation_english_names[ u'/r/HasPrerequisite'] = "has prerequesite of"
conceptnet_relation_english_names[ u'/r/wordnet/adjectivePertainsTo'] = "is an adjective that pertains to"
conceptnet_relation_english_names[ u'/r/MadeOf'] = "is made of"
conceptnet_relation_english_names[ u'/r/Entails'] = "entails"
conceptnet_relation_english_names[ u'/r/AtLocation'] = "is at location"
conceptnet_relation_english_names[ u'/r/Attribute'] = "is the attribute of"
conceptnet_relation_english_names[ u'/r/Desires'] = "desires"
conceptnet_relation_english_names[ u'/r/NotIsA'] = "is not"
conceptnet_relation_english_names[ u'/r/CapableOf'] = "is capable of"
conceptnet_relation_english_names[ u'/r/HasA'] = "has a"
conceptnet_relation_english_names[ u'/r/SymbolOf'] = "is a symbol of"
conceptnet_relation_english_names[ u'/r/CausesDesire'] = "causes desire"
conceptnet_relation_english_names[ u'/r/NotDesires'] = "does not desire"
conceptnet_relation_english_names[ u'/r/MotivatedByGoal'] = "is motivated by the goal of"
conceptnet_relation_english_names[ u'/r/CreatedBy'] = "is created by"
conceptnet_relation_english_names[ u'/r/DefinedAs'] = "is defined as"
conceptnet_relation_english_names[ u'/r/HasSubevent'] = "has the subevent of"
conceptnet_relation_english_names[ u'/r/HasFirstSubevent'] = "has the first subevent of"
conceptnet_relation_english_names[ u'/r/HasLastSubevent'] = "has the last subevent of"
conceptnet_relation_english_names[ u'/r/SimilarSize'] = "has a size similar to"
conceptnet_relation_english_names[ u'/r/LocatedNear'] = "is located near"
conceptnet_relation_english_names[ u'/r/wordnet/adverbPertainsTo'] = "is an adverb that pertains to"
conceptnet_relation_english_names[ u'/r/NotHasProperty'] = "does not have the property of"
conceptnet_relation_english_names[ u'/r/NotCapableOf'] = "is not capable of"
conceptnet_relation_english_names[ u'/r/Derivative'] = "is a derivative of"
conceptnet_relation_english_names[ u'/r/InheritsFrom'] = "inherits from"
conceptnet_relation_english_names[ u'/r/wordnet/participleOf'] = "is the participle of"
conceptnet_relation_english_names[ u'/r/NotHasA'] = "does not have"
conceptnet_relation_english_names[ u'/r/HasPainIntensity'] = "has pain intensity"
conceptnet_relation_english_names[ u'/r/NotMadeOf'] = "is not made of"
conceptnet_relation_english_names[ u'/r/HasPainCharacter'] = "has pain character"
