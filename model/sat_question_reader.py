import sys
import os
from model.question import Question

'''
Iterator over the SAT analogies
''' 
class SATReader:
	def __init__(self):
		self.f = open(os.getcwd()+'/data_questions/SAT-package-V3.txt')

	def __iter__(self):
		return self

	def next(self):
		TOTAL_ANSWERS = 5
		sat_q = None
		while not sat_q:
			l = self.f.readline()

			# EOF
			if not l:
				break

			# skip comments
			if l[0] == '#':
				continue

			if len(l) == 2:
				# description, skip
				l = self.f.readline()
				# question
				t = self.f.readline().split(' ')
				q = ','.join([t[0],t[1]])
				a=[]
				# answers
				for k in range(5):
					t = self.f.readline().split(' ')
					if t[0]!='no' and t[1]!='choice':
						a.append(  ','.join([t[0],t[1]]) )
					else:
						TOTAL_ANSWERS-=1
				s = self.f.readline()[0]
				
				sat_q = Question( q, a, s)
		if sat_q is not None:	
			assert len(sat_q.answers)==TOTAL_ANSWERS
		return sat_q

	@staticmethod
	def get_entire_question( first, second ):
                qreader = SATReader()
                for q in qreader:
                        if not q:
                                break
                        #qfirst,qsecond = map(wnlemma.lemmatize, q.question.split(','))
                        qfirst,qsecond = q.question.split(',')
                        if (qfirst==first) and (qsecond==second):
                                return q
		return None

