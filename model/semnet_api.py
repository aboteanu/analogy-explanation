from model.caches.everything_per_task import EverythingPerTaskCache
from model.conceptnet_api import conceptnet_api, conceptnet_edges_between, uri_lemma
from model import wordnet_api
from model import cyc_api
from model.conceptnet_static import conceptnet_relation_english_names
from model.wordnet_static import wordnet_relation_english_names

def connected_concepts( input_name, net ):
	if net == 'CN':
		return conceptnet_api( input_name )
	elif net == 'WN':
		return wordnet_api.node_expansion( input_name )
	elif net == 'CYC':
		return cyc_api.node_expansion( input_name )
	else:
		raise ValueError

def edges_between_concepts( c1, c2, net ):
	if net == 'CN':
		return conceptnet_edges_betwen( c1, c2 )
	elif net == 'WN':
		pass
	elif net == 'CYC':
		pass
	else:
		raise ValueError
 
def edge_lemma( uri, net ):
	if net == 'CN':
		return uri_lemma( uri )
	elif net == 'WN':
		return uri
	elif net == 'CYC':
		return uri
	else:
		raise ValueError

def relation_english_names( rel, net ):
	if net == 'CN':
		return conceptnet_relation_english_names[rel]
	elif net == 'WN':
		return wordnet_relation_english_names[rel]
	elif net == 'CYC':
		return 'related to'
	else:
		raise ValueError
