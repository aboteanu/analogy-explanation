import threading 

class WorkerThread(threading.Thread):
        def __init__(self, target, name, *args):
                self._target = target
                self._args = args
                threading.Thread.__init__(self)

                self.name = name

        def run(self):
                self.result = self._target(*self._args)

