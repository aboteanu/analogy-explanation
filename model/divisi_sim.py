from csc import divisi2

# init divisi
matrix = divisi2.network.conceptnet_matrix('en')
concept_axes, axis_weights, feature_axes = matrix.normalize_all().svd(k=10)
divisi_sim_compare = divisi2.reconstruct_similarity(concept_axes, axis_weights, post_normalize=True)

def concept_pair_sim( a, b ):
	return divisi_sim_compare.entry_named( a, b )

def concept_sim( a, k=500 ):
	# TODO threshold by weight in addtion to top k?
	result = list()
	cw = divisi_sim_compare.col_named( a ).top_items( k )
	for (c,w) in cw:
		if c!=a and len(c.split(' '))==1:
			result.append(c)
	return result
