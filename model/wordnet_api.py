import itertools
from caches.everything_per_task import EverythingPerTaskCache
from nltk.corpus import wordnet as wn

cache = EverythingPerTaskCache()

def clear_cache():
	cache.endOfTaskCleanup()

def node_expansion( x ):
	'''
	First check the cache, if not there go to WordNet

	>>> assert len(node_expansion('water bottle')) > 0
	'''
	# replace spaces with _ for stuff like "dinner plate" -> "dinner_plate"
	word = '_'.join( x.split(' ') )

	if word in cache:
		data = cache[word]
		return data
	else:
		data = _node_expansion( word )
		cache[ word ] = data
		return cache[ word ]

def _node_expansion(x):
	'''
	Return all the words found in the related synsets, hyponyms and hypernyms

	>>> assert len(node_expansion('water bottle')) > 0
	'''
	if x in cache:
		data = cache[x]
		return data
	else:
		result = list()

		synsets = wn.synsets(x)
		related = set(list(itertools.chain(*map(lambda s: s.lemma_names(), synsets))))
		result += make_synset_edges( x, related, u'wn_synset' )

		synsets  = list(itertools.chain(*map(lambda s: s.hyponyms(), synsets)))
		related = set(list(itertools.chain(*map(lambda s: s.lemma_names(), synsets))))
		result += make_synset_edges( x, related, u'wn_hyponym' )

		synsets = list(itertools.chain(*map(lambda s: s.hypernyms(), synsets)))
		related = set(list(itertools.chain(*map(lambda s: s.lemma_names(), synsets))))
		result += make_synset_edges( x, related, u'wn_hypernym' )
	
		if len( result ) > 0:	
			cache[x] = result 
			return result
		else:
			return list()

def make_synset_edges( x, related, relname ):
	result = list()
	for w in related:
		e=dict()
		e[u'rel'] = relname
		e[u'weight'] = 1.0
		e[u'start'] = x
		e[u'end'] = w
		result.append( e )
	return result

if __name__=='__main__':
	import doctest
	doctest.testmod()
