import itertools
import xml.etree.ElementTree
import os
import pickle
from caches.everything_per_task import EverythingPerTaskCache
cyc_data_path = '/home/adrian/analogy-explanation/model/data/'
cache = EverythingPerTaskCache()
cyc_idToWordsMap = dict()
cyc_wordToIdMap = dict()
cyc_idToRelatedIds = dict()

def clear_cache():
	cache.endOfTaskCleanup()

def init():
	'''
	Initialize the id-words, word-id,id-ids mappings from the opencyc owl data file.
	'''
	global cyc_idToWordsMap
	global cyc_wordToIdMap
	global cyc_idToRelatedIds

	owl_file = cyc_data_path + 'owl-export-unversioned.owl'
	assert os.path.isfile( owl_file )
	e = xml.etree.ElementTree.parse(owl_file).getroot()
	#e = xml.etree.ElementTree.parse('./cyc_api/data/test.owl').getroot()
	l = e._children

	for c in l:
		id = c.attrib.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about')
		if id == None or len(id) == 0 or id.__contains__('http'):
			continue
		if cyc_idToWordsMap.get(id) != None:
			continue
		cl = c._children
		relatedIds = set()
		words = set()
		for x in cl:
			langAttr = x.attrib.get('{http://www.w3.org/XML/1998/namespace}lang')
			resourceAttr = x.attrib.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource')
			if x.tag != '{http://sw.cyc.com/CycAnnotations_v1#}label' \
					and x.tag != '{http://www.w3.org/2000/01/rdf-schema#}comment' \
					and langAttr == 'en':
				#print x.text
				words.add(x.text)
			elif resourceAttr != None and len(resourceAttr) > 0 and not resourceAttr.__contains__('http'):
				#print resourceAttr
				relatedIds.add(resourceAttr)
			cyc_idToWordsMap[id] = words
			cyc_idToRelatedIds[id] = relatedIds
		for w in words:
			cyc_wordToIdMap[w] = id
	#pickle.dump( cyc_idToWordsMap, open( cyc_data_path +'cyc_idToWordsMap.pickle', 'w' ) )
	#pickle.dump( cyc_wordToIdMap, open( cyc_data_path +'cyc_wordToIdMap.pickle', 'w' ) )
	#pickle.dump( cyc_idToRelatedIds, open( cyc_data_path +'cyc_idToRelatedIds.pickle', 'w' ) )

def node_expansion(x):
	'''
	Return all the words that the related ids refer to

	>>> assert len(node_expansion('red')) > 0
	'''
	global cyc_idToWordsMap
	global cyc_wordToIdMap
	global cyc_idToRelatedIds

	if x in cache:
		data = cache[x]
		return data
	else:
		# check if the word map needs to be loaded
		if len( cyc_wordToIdMap ) == 0 or len( cyc_idToWordsMap ) == 0:
			#if os.path.isfile( cyc_data_path +'cyc_idToWordsMap.pickle' ) and \
			#	os.path.isfile( cyc_data_path +'cyc_wordToIdMap.pickle' ) and \
			#	os.path.isfile( cyc_data_path +'cyc_idToRelatedIds.pickle' ) :
			#	idToWordsMap = pickle.load( open( cyc_data_path +'cyc_idToWordsMap.pickle', 'r' ) )
			#	cyc_wordToIdMap = pickle.load( open( cyc_data_path +'cyc_wordToIdMap.pickle', 'r' ) )
			#	idToRelatedIds = pickle.load( open( cyc_data_path +'cyc_idToRelatedIds.pickle', 'r' ) )
			#else:
			init()

		if cyc_wordToIdMap.__contains__(x):
			id = cyc_wordToIdMap[x]
			sameMeaningWords = cyc_idToWordsMap[id]
			relatedIds = cyc_idToRelatedIds[id]
			words = map(lambda x, cyc_idToWordsMap=cyc_idToWordsMap: cyc_idToWordsMap[x] \
				if cyc_idToWordsMap.__contains__(x) else None, relatedIds)
			words += list(sameMeaningWords)
			words = map(lambda x:x if x != None and x.__len__() > 0 else [], words)
			words = filter(None, words)
			# merge all subsets from words into a single set
			result = set()
			for x in words:
				if type(x)==str:
					result.add(x)
				elif type(x)==set:
					result=result.union(x)
				else:
					raise Exception
			edges_result = make_cyc_edges( x, result, u'opencyc')
			cache[x] = edges_result
			return edges_result
		cache[x] = set()
		return set()

def make_cyc_edges( x, related, relname ):
        result = list()
        for w in related:
                e=dict()
                e[u'rel'] = relname
                e[u'weight'] = 1.0
                e[u'start'] = x
                e[u'end'] = w
                result.append( e )
        return result

if __name__=='__main__':
	import doctest
	doctest.testmod()
