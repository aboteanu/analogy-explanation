from nltk.corpus import wordnet as wn

def wordnet_path_sim( a, b ):
	# TODO this only gets the first synset, [0]
	syn_a = wn.synsets( a )[0]
	syn_b = wn.synsets( b )[0]
	return syn_a.path_similarity( syn_b )
