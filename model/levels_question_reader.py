import sys
import os
import glob
import re
from model.question import Question
import string

'''
Iterator over the Level data analogies
'''
class LevelsReader:
	def __init__(self, level):
		if level <= 12 and level >= 1:
			self.level = level
		else:
			return None
		path = os.getcwd()+'/data_questions/EnglishForEveryone/Level_1_12_Analogies'
		self.files = glob.glob(os.path.join(path, 'Level_'+str(level)+'_*.csv'))
		self.file_index = 0
		self.f = open(self.files[self.file_index], 'rt')

	def __iter__(self):
		return self

	def check_num_answers( self, q ):
		if self.level == 1:
			assert len(q.answers)==3
		elif self.level >= 2 and self.level <= 9:
			assert len(q.answers)==4
		else:
			assert len(q.answers)==5

	def next(self):
		lvl_q = None
		while not lvl_q:
			l = self.f.readline()

			# EOF
			if not l:
				if self.file_index < len(self.files)-1:
					self.f.close()
					self.file_index += 1
					self.f = open( self.files[self.file_index], 'rt')
				else:
					break
			# skip comments
			if l and l[0] == '#':
				continue

			# parse new file fmt
			# check if it the first line of a question
			# 11)) or 1100))
			m = re.search( '[0-9]+\)\)', l)
			if m is not None:
				if len( m.group(0) ) == 4:
					question_id = m.group(0)[1:3] # eg 11)) => 1)
				elif len( m.group(0) ) == 6:
					question_id = m.group(0)[1:3]+m.group(0)[-1] # eg 1100)) => 10)
				else:
					raise Exception("Wrong Question Format "+l)
			else:
				continue

			# from the same line, get the question words
			q = re.search( '[A-Z]+ *: *[A-Z]+', l )
			if q is not None:
				q = ','.join( map( string.strip, (q.group(0).lower()).split(':') ) )
			else:
				raise Exception("Wrong Question Format "+l)

			# read next line, determine the question type
			a=[]
			t = self.f.readline()
			m = re.search( '[A-Z]+\)\)', t )
			if m is not None:
				# word pair type question, up to 5 possible answers
				for kans in range(1, 6):
					ans = re.search( '[a-z]+ *: *[a-z]+', t )
					if not ans:
						break # some questions have 4 instead of 5 answers
							# just stop, instead of panicking
					a.append( (ans.group(0).lower()).replace(' : ', ',') )
					t=self.f.readline()
			else:
				# single word, get this and add to all other answer
				ans = re.search( '[A-Z]+ *:', t )
				if not ans:
					raise Exception("Wrong Question Format "+t)
				# construct the answers in the two word format
				answer_first_word = (ans.group(0).split(':')[0]).strip().lower()
				# 3 or 4 possible answers
				for kans in range(1,5):
					t=self.f.readline().lower()
					ans = re.search( '[a-z]+.. *[a-z]+', t )
					if not ans:
						break 
						#raise Exception("Wrong Question Format "+t)
					answer_second_word = (ans.group(0).split('..')[1]).strip().strip().lower()
					a.append( ','.join( [answer_first_word, answer_second_word] ))

			# look for the correct answer at the end of the file
			return_to_location = self.f.tell()
			solution = None
			for t in self.f:
				# answers have only one parantheses
				sol = re.search('[0-9]+\) *[A-Z]', t)
				if sol is None:
					continue
				if t.split(' ')[0] == question_id:
					solution = (sol.group(0).split(question_id)[1]).strip().lower()
					break

			# create the question if the solution was found
			if solution:
				lvl_q = Question( q, a, solution )
			else:
				raise Exception("No Solution Found for "+q)

			# return to the end of the question in the file
			self.f.seek( return_to_location, 0 )
		if lvl_q is not None:
			self.check_num_answers(lvl_q)				
		return lvl_q

	@staticmethod
	def get_entire_question( first, second ):
		for level in range(1,13):
			qreader = LevelsReader(level)
			for q in qreader:
				if not q:
					break
				#qfirst,qsecond = map(wnlemma.lemmatize, q.question.split(','))
				qfirst,qsecond = q.question.split(',')
				if qfirst==first and qsecond==second:
					return q

		return None
