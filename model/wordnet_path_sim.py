import itertools
from nltk.corpus import wordnet as wn

def wordnet_pair_sim( a, b ):
	ss1 = wn.synsets(a)
	ss2 = wn.synsets(b)
	try: 
		sim = max(s1.path_similarity(s2) for (s1, s2) in itertools.product(ss1, ss2))
	except ValueError:
		print (a,b)
		return 0
	if not sim:
		sim = 0.0
	return sim

