import sys
import os

solution_char_to_int = {
		'a' : 1,
		'b' : 2,
		'c' : 3,
		'd' : 4,
		'e' : 5
	}

class Question:
	def __init__(self, question, answers, solution):
		self.id = None
		self.level = None
		self.question = question
		self.answers = answers
		self.solution = solution_char_to_int[solution]

	def print_question( self ):
		print self.question
		for a in self.answers:
			print a
		print self.solution

