import nltk
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
import re
from model.conceptnet_static import NWORD_CONCEPTS

wnlemma = WordNetLemmatizer()
english_stopwords = stopwords.words('english')


def check_pos( w, pos ):
	'''
	Because we only use words, regular tagging doesn't work
	For example, "swim" is also a noun, but probably it makes more sense
	to consider it first as a verb
	
	Thus, get all synsets from Wordnet
	Find the lemma that matches the word and the pos
	Different question generators will attempt different pos per word
	This only checks that the word also has a meaning if considered 
	as the part of speech given by pos

	>>> check_pos( 'red', 'n' )
	True
	>>> check_pos( 'red', 'a' )
	True
	>>> check_pos( 'swim', 'v' )
	True
	>>> check_pos( 'good', 'v' )
	False
	>>> check_pos( 'duck', 'n' )
	True
	>>> check_pos( 'duck', 'v' )
	True
	>>> check_pos( 'slowly', 'adv' )
	True
	>>> check_pos( "ogi3oi", 'n' )
	False
	'''
	ssets = []
	if pos == 'n':
		ssets = wn.synsets(w, pos=wn.NOUN)
	elif pos == 'v':
		ssets = wn.synsets(w, pos=wn.VERB)
	elif pos == 'a':
		ssets = wn.synsets(w, pos=wn.ADJ)
	elif pos == 'adv':
		ssets = wn.synsets(w, pos=wn.ADV)
	if not ssets:
		return False
	return True

def infinitive(x):
	return 'to '+x

def is_number(x):
	return x in ['zero','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve']

def is_pronoun(x):
	return x in ['he', 'she', 'it', 'someone', 'something']

def is_bad_word(x):
	# Why are kids dumb
	# What do dogs s*** 
	# What else is natural besides s***
	# Why do bones break
	# Why is pain a part of life
	# Can a child be weird
	# Can nothing be true
	# Why do children hate
	# Why is the universe made of strings
	# Can life be hard
	return x in ['dumb', 'stupid', 'shit', 'weird', 'hate']

def is_person(x):
	return x in ['girl', 'boy', 'woman', 'man', 'elder', 'guardian', 'mom', 'mommy', 'dad', 'daddy', 'mother', 'father', 'son', 'daughter', 'nick', 'henry', 'david']

def revert_from_lemma( word, retrieved_word ):
        # word has changed
        if word==wnlemma.lemmatize(retrieved_word):
                # revert to form before lemma, as found in question
                return retrieved_word
        else:
                # can't match word with pre-lemma form
                print 'Lemma error', retrieved_afirst, afirst
                assert False

def get_answer_before_lemma( answers, afirst, asecond ):
        for a in answers:
                x,y = a.split(',')
                if x!=afirst or y!=asecond:
                        if wnlemma.lemmatize(x)==afirst and wnlemma.lemmatize(y)==asecond:
                                return x,y
        return afirst, asecond

def useful_word( w ):
	if not w:
		return False

	if len(w.split('_'))>NWORD_CONCEPTS:
		return False

	if w in english_stopwords:
		return False

	if len(w)<3:
		return False

	return True

def is_ascii(s):
	try:
		s.decode("ascii")
		return True
	except:
		return False

def decode( inword ):
	'''
	Process task names such as "PlaceFork" to separate words i.e. "put fork"

	>>> print decode( 'plate' )
	plate
	>>> print decode( 'Plate' )
	plate
	>>> print decode( 'setPlace' )
	set place
	>>> print decode( 'SetPlace' )
	set place
	>>> print decode( 'setPlace1' )
	set place
	>>> print decode( 'Object.Fork' )
	fork
	'''
	if not inword:
		return inword

	if '.' in inword:
		word = inword.split('.')[-1]
	else:
		word = inword

	separate = ''
	for l in word[::-1]: # reverse string and traverse
		if l.isdigit():
			continue # ignore numbers
		if l.isupper():
			separate += l.lower()+' '
		else:
			separate += l

	string = (separate[::-1]).strip() # reverse back

	return string
