from model.worker_thread import WorkerThread
from sse.subgraph import Subgraph
from sse.prune_graph import prune_non_paths
from sse.relational_similarity import relational_similarity
from sse.explain import explain_solution
from main import SEMANTIC_NETWORK

def get_subgraph( pair ):
	'''
	Start from a pair of words, get the raw graph, prune it
	and then optionally compress all is-a relations
	>>> s = get_subgraph( (u'stone', u'house') )
	>>> assert s is not None
	>>> assert s.connected() is not None
	>>> s = get_subgraph( (u'goose', u'flock') )
	>>> assert s is not None
	>>> assert s.connected() is not None
	'''
	assert pair is not None 
	sn = Subgraph( )
	sn.create_subgraph( pair )
	if sn.connected() is None:
	#	print '###', pair, 'NOT CONNECTED'
		return None
	snp = prune_non_paths( sn )
	assert snp is not None
	assert snp.connected() is not None
	assert snp.start_words is not None
        #print 'subgraph for', pair, snp.print_graph()
	return snp

def answer_question(question):
	qwords = question.question.split(',') # question
	qans = list()
	n = len( question.answers )
	question_id = question.id
	for i in range(n):
		pair = question.answers[i].split(',')
		qans.append(pair)

	snps = dict()
	snps['0'] = get_subgraph( qwords )
	for k in range( len( qans ) ):
		snps[str(k+1)] = get_subgraph( qans[k] )
	# create threads and add to list
	#threads=list()
	#tq = WorkerThread( get_subgraph, '0', qwords )
	#threads.append(tq)
	#for k in range(len(qans)):
	#	ta = WorkerThread( get_subgraph, str(k+1), qans[k] )
	#	threads.append(ta)
	# start all threads
	#TODO re-enable multithreading #for t in threads:
	#for t in threads:
	#	t.start()
	#	t.join(500)
	#for t in threads:
	#	if t.isAlive():
	#		print 'Timeout',t.name, t._args
	#		continue
	#	snps[t.name] = t.result

	result = dict()
	direct_max_mark = -1
	onehop_max_mark = -1
	k=1
	solution_path_pair_direct = None
	solution_path_pair_onehop = None
	result['answer_direct'] = None
	result['answer_onehop'] = None
	for snps_i in snps:
		if snps_i == '0':
			continue
		if '0' not in snps:
			continue
		if snps[ snps_i ] is None:
			continue
		(best_direct_path, direct_mark), (best_onehop_path, onehop_mark) = relational_similarity( snps['0'], snps[ snps_i ] )
		if direct_mark > direct_max_mark:
			direct_max_mark = direct_mark
			solution_path_pair_direct = best_direct_path
			result['answer_direct'] = (k, snps[snps_i].start_words, direct_max_mark)
		if onehop_mark > onehop_max_mark:
			onehop_max_mark = onehop_mark
			solution_path_pair_onehop = best_onehop_path
			result['answer_onehop'] = (k, snps[snps_i].start_words, onehop_max_mark)
		k+=1

	if result['answer_direct'] is not None:
		print ','.join( map( str, [ question.level, question.question, SEMANTIC_NETWORK + '_direct',
				result['answer_direct'][2],result['answer_direct'][0], question.solution] ) )
	else:
		print ','.join( map( str, [ question.level, question.question, SEMANTIC_NETWORK + '_direct',
				'0', 'None', question.solution] ) )
	if result['answer_onehop'] is not None:
		print ','.join( map( str, [ question.level, question.question, SEMANTIC_NETWORK + '_onehop',
				result['answer_onehop'][2],result['answer_onehop'][0], question.solution] ) )
	else:
		print ','.join( map( str, [ question.level, question.question, SEMANTIC_NETWORK + '_onehop',
				'0', 'None', question.solution] ) )

	#print question.question, ' --- '
	#if result['answer_direct'] is not None:
	#	print '   choice, words, mark', result['answer_direct']
	#else:
	#	print '    ', 'None_direct'
	#if result['answer_onehop'] is not None:
	#	print '    choice, words, mark', result['answer_onehop']
	#else:
	#	print '    ', 'None_onehop'

	#result['expl_direct']=None
	#result['expl_onehop']=None
	#if solution_path_pair_direct is not None:
	#	explsolp = explain_solution( solution_path_pair_direct )
	#	result['expl_direct'] = explsolp
	#if solution_path_pair_onehop is not None:
	#	explsolp = explain_solution( solution_path_pair_onehop )
	#	result['expl_onehop'] = explsolp

