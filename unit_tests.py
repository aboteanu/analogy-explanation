if __name__=='__main__':
	import doctest
	import sys

	import model.conceptnet_api as conceptnet_api
	import model.wordnet_api as wordnet_api
	import model.cyc_api as cyc_api
	import sse.node as node
	import sse.subgraph as subgraph
	import sse.prune_graph as prune_graph
	import model.nlp as nlp

	import answer_sse

	doctest.testmod( nlp )
	doctest.testmod( conceptnet_api )
	doctest.testmod( wordnet_api )
	doctest.testmod( cyc_api )

	doctest.testmod( node )
	doctest.testmod( prune_graph )
	doctest.testmod( subgraph )

	doctest.testmod( answer_sse )

