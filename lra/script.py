import nltk, re, pprint
import itertools
import os
import re

regex = re.compile('[^a-z]')

def clean_word( x ):
  return regex.sub( '', x.lower() )

def lemmas( f ):
  for l in f:
    word = map( clean_word, re.split(' ', l.encode("ascii")) )
    lem = map(nltk.WordNetLemmatizer().lemmatize, [x for x in word if x is not None ] ) 
    yield lem

def find_phrases(phrases, pairs, gen4):
  for i in gen4:
    for ( word1, word2 ) in pairs:
      if ( word1, word2 ) not in phrases:
        phrases[ ( word1, word2 ) ] = list()
        phrases[ ( word1, word2 ) ].append( list() ) # new empty phrase

      #if word2 in phrases[ ( word1, word2 ) ]:
      #  phrases[ ( word1, word2 ) ] = list()
      #  print "oops, no end"

      if len( phrases[(word1, word2)]) >0 and len(phrases[ ( word1, word2 ) ][-1]) < 6:
        if i == word1:
          phrases[ (word1, word2)][-1].append( i )
          #print "found a word!  ss" + str(i)
        elif i == word2 and len( phrases[ (word1, word2) ][-1] ) > 0:
          phrases[ (word1,word2) ][-1].append( i ) # close this phrase
          phrases[ (word1,word2) ].append( list() ) # start new phrase
          #print "we have atleast 1 end"
          #yield phrase
        elif len(phrases[ (word1,word2) ]) > 0:
          phrases[ (word1, word2) ][-1] = phrases[ (word1, word2) ][-1] + [i]
          #print "starting new or continuing   " + str(phrase)
      elif len( phrases[ (word1, word2) ] ) > 0:
          phrases[ (word1, word2) ].pop() 
        #print "maybe next time"
  return phrases

def find_patterns(twodlist):
  pl = []                                #pattern list for one particular phrase
  for i in range(len(twodlist)-1):
    for m in range(len(twodlist[i])-1):
      k = m[1:-1]
      new_list = itertools.permutations(k)
    #gives permutations of the ineer words so that wildcard can be inserted

    for j in new_list:
      if l == []: #one pattern per permutation
        for n1 in range(len(j)-1):
          if j[n1] != k[n1]:
            l= l + ['*']
          else:
            l = l + j[n1]
        pl = pl + [[m[0]] + l + [m[-1]]]
    return pl

def keep_patterns(plist, gen6):  #plist is a list of phrases, will also be a 2-D list
  for patlist in gen6:    #patternlist for a particluar phrase
    l =[]
    for i in patlist:
      count = 0
      for j in plist:
        mach = [a for a, b in zip(j[1:-1],i[1:-1]) if a!=b] #word is in the list, not in the pattern
                                                            # first and last word are the same anyway
        if len(mach) == i.count('*'): #number of unmatched words = no of *'s
          count = count + 1
      yield (i, count) #count how many phrases that each generator (pattern) hit

