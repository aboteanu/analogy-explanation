import numpy
import scipy
import math
from scipy import linalg
from scipy.sparse import csr_matrix
import script

#get rows from word pairs
#get cols from number of patterns

#rows = no. of rows
#cols = no. of columns

def make_row_cols(rmapping, cmapping):
  row = []
  col = []
  data = []
  for wpair in rmapping:
    for patt in cmapping:
      if wpair[0] == patt[0] and wpair[1] == patt[-1]:
        row = row + rmapping[wpair]
        col = col + cmapping[patt][0]
        data = data + cmapping[patt][1]

  return (row, col, data)


def make_matrix(rcdtup):
  m_row = rcdtup[0]
  m_col = rcdtup[1]
  m_data =rcdtup[2]

  return csr_matrix((m_data, (m_row, m_col)), shape = (len(m_row), len(m_col)))


def do_log_stuff(inputmatrix):
  i = 0
  arr = inputmatrix.toarray()
  rc = arr.shape
  while i < rc[0] :
    j = 0
    while j< rc[1]:
      el = arr[i][j]
      if el > 0 :
        arr[i][j] = -1*el*(math.log(el))
      elif el == 0:
        arr[i][j] = 0
      else:
        el = (-1)*el
        arr[i][j] = -1*el*(math.log(el))
      j = j+ 1
    i = i+ 1
  return arr

def apply_svd(matrx):
  d = {}
  u = linalg.svd(matrx)[0]
  s = linalg.svd(matrx)[1]
  v = linalg.svd(matrx)[2]
  d['uk'] = u[0:, 0:k]
  d['sk'] = s[0:, 0:k]
  d['vt'] = v
  return d

def get_projection(svddict):
  u = svddict['uk']
  s = svddict['sk']
  return u*s

def cosine_matrix(mat):
  return numpy.cos(mat)

def relational_similarity(cosmatrix):
  cosarr = []
  for i in cosmatrix:
    cosarr = cosarr + [numpy.sum(i)/len(i)]

  return cosarr
