import syn
import script
import mapping
import matrix
import re
import glob
import gzip

def lra_build_space( wplist ):
  '''
  wplist is a list of all word pairs that will be used, not just a question
  '''
  lra_data_files = glob.glob('/media/adrian/data/waterloo-corpus/*.dat.gz')

  altlist = list()
  for wpair in wplist:
    altlist = altlist + syn.make_pairs( wpair )   #gives a list of alternatives for every word pair

  #find phrases and count
  i = 0
  phraseLists = dict()
  for filename in lra_data_files:
    if filename[-6:] == 'dat.gz':
      f = gzip.open( filename )
    elif filename[-3:] == 'dat':
      f = open( filename )
    else:
      print 'Skipping', filename, 'unknown type; need .dat.gz or .dat'
    lemmaGen = script.lemmas(f)    #add file name
    phraseLists = script.find_phrases( phraseLists, altlist, lemmaGen)
    i = i + 1
    f.close()

  phraseList = list()
  for p in phraseLists.values():
    if len( p ) > 0:
      phraseList += p
  phraseList = list( set( phraseList ))

  finalWordTuple = []#list that goes to row mapping

  #finishing step-2, finalizig alternatives that go forward
  for j in range(len(phraseList)-1):
    for m in range(len(phraseList[j])-1):
      for n in range(len(altlist[j])-1):                         #altlist and phrase list go together
        if altlist[j][n][0] == phraseList[j][m][0]:       #first word in pair = first word in phrase
          if altlist[j][n][1] == phraseList[j][m][-1]:    #second word in pair = last word in phrase
            finalWordTuple = finalWordTuple + altlist[j][n]

  patternGen = script.find_patterns(phraseList)
  patcountGen = script.keep_patterns(phraseList, patternGen)   #pattern, count as generators

  # not expecting 4000 patterns because words usually have a hard time matching each other
  #can put in a provision for discarding a few patterns if it takes too much time or space

  #preparing to set up the matrix
  rowMapping = mapping.map_row(finalWordTuple)
  colMapping = mapping.map_col(patcountGen)

  #setting up the matrix
  rcd = matrix.make_row_cols(rowMapping, colMapping)
  matrix1 = matrix.make_matrix(rcd)

  #log and entropy calulations
  logmatrix = matrix.do_log_stuff(matrix1)

  #apply svd
  svdDict= matrix.apply_svd(logmatrix)

  #get projection
  projmatrix = matrix.get_projections(svdDict)

  #step 11 - evaluating alternatives
  cosmat = matrix.cosine_matrix(projmatrix)

  cosmatfile = open( 'lra_cosmat.pickle', 'w') 
  pickle.dump( cosmat, cosmatfile )
  cosmatfile.close()

  finalwordtuplefile = open( 'lra_finalwordtuple.pickle', 'w' )
  pickle.dump( finalWordTuple, finalwordtuplefile )
  finalwordtuplefile.close()

  rowmapfile = open( 'lra_rowmap.pickle', 'w' )
  pickle.dump( rowMapping, rowmapfile )
  rowmapfile.close()

def lra_answer_question( wplist ):
  cosmatfile = open( 'lra_cosmat.pickle', 'r') 
  cosmat = pickle.load( cosmatfile )
  cosmatfile.close()
  finalwordtuplefile = open( 'lra_finalwordtuple.pickle', 'r' )
  finalWordTuple = pickle.load( finalwordtuplefile )
  finalwordtuplefile.close()
  rowmapfile = open( 'lra_rowmap.pickle', 'r' )
  rowMapping = pickle.load( rowmapfile )
  rowmapfile.close()

  #finding the cosine value of original question a:b
  rnum = finalWordTuple.find(wplist[0])
  #finiding actual solution
  finalCos = matrix.relational_similarity(cosmat)
  origcos = finalCos[rnum]

  mnum = max(cosarr)
  ind = cosarr.find(mnum)
  answer = None
  if mnum >= origcos:
    for key in rowMapping:
      if ind == rowMapping[key] and key != wplist[0]:
        answer = key

  if answer:  
    print ','.join( map( str, [ level, wplist[0][0], wplist[0][1], 'LRA',
			mnum, answer, solution ] ) )
  else:
    print ','.join( map( str, [ level, wplist[0][0], wplist[0][1], 'LRA',
			'0', 'None', solution ] ) )

