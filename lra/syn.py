#Divyansha 2016
#synonyms

import nltk
from nltk.corpus import wordnet as wn
import itertools

def synonym_list(word):
  ''' find the synonym_list for a word'''
  synList = []
  for i in wn.synsets(word):
    for lemma in i.lemmas():
      synList = synList + [str(lemma.name())]
  return synList

def syn_list_no_dups(synlist):
  '''removes duplicates from the list '''
  flist = []
  for word in synlist :
    #if (word in flist) == False :     #and (len(flist)<=10)
    if flist.count(word) == 0and (len(flist)<5):
      flist = flist + [word]
  return flist

def make_pairs1((word1, word2)):
  '''make pairs in the form A':B'''
  a = synonym_list(word1)
  b = syn_list_no_dups(a)
  print b

  reqlist = []
  for word in b:
      reqlist = reqlist + [(word,word2)]

  return reqlist

def make_pairs2((word1,word2)):
  ''' make pairs of the form A: B' '''
  a = synonym_list(word2)
  b = syn_list_no_dups(a)

  reqlist=[]
  for word in b:
      reqlist = reqlist + [(word1, word)]

  return reqlist


# Adrian
def make_pairs( (word1, word2) ):
  '''
  make synonym pairs from all possible combinations of synonyms
  '''
  syn1 = synonym_list( word1 )
  syn2 = synonym_list( word2 )

  syn1.append( word1 )
  syn2.append( word2 )

  syn1 = [ x for x in syn1 if '_' not in x ]
  syn2 = [ x for x in syn2 if '_' not in x ]
 
  return list( itertools.product( syn1, syn2 ) )
