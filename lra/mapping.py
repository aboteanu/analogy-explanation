#mapping for word pairs and row numbers
def map_row(tupist):  #tublist is word list
  i = 0
  mapping = {}
  for tup in tupist:
      mapping[tup] = i
      i = i + 1
      mapping[(tup[1], tup[0])] = i
      i = i + 1

  return mapping

#map pattern and column numbers
def map_col(tupgen):
  cmapping = {}
  n = 0
  for i in tupgen:
    cmapping[i[0]] = (n, i[1])
    n = n+1
    np = i[-1] + i[1:-1] + i[0]
    cmapping[np] = (n, 0)
    n = n+1

  return cmapping



