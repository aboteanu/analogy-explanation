import csv

cyc_csvfile = open( 'results/answers_cyc.csv', 'rb' ) 
wn_csvfile = open( 'results/answers_wn.csv', 'rb' ) 
cn_csvfile = open( 'results/aaai15/answers_levels_sat_1000.log', 'rb' ) 

def read_results( csvfile ):
	direct_question_result = dict()
	onehop_question_result = dict()
	reader = csv.reader( csvfile, delimiter=',' )
	for row in reader:
		level = row[0]
		w1 = row[1]
		w2 = row[2]
		net_method = row[3]
		score = row[4]	
		answer = row[5]
		solution= row[6]

		if level == "level":
			continue
		
		attempt = 0
		correct = 0
		if answer != "None":
			attempt = 1
		if answer != "None" and answer==solution:
			correct = 1

		if 'direct' in net_method:
			direct_question_result[ (w1, w2) ] = (attempt, correct)
		if 'onehop' in net_method:
			onehop_question_result[ (w1, w2) ] = (attempt, correct)

	csvfile.close()
	return direct_question_result, onehop_question_result

def read_cn_results( csvfile ):
	direct_question_result = dict()
	onehop_question_result = dict()
	reader = csv.reader( csvfile, delimiter=',' )

	for row in reader:
		if len(row) == 0:
			continue
		level = row[1]
		w1 = row[3]
		w2 = row[4]
		score = 0
		result = row[-1]


		attempt = 0
		correct = 0
		if result!='-1':
			attempt = 1 
		if result=='1':
			correct = 1

		if row[6] == 'd':
			direct_question_result[ ( w1, w2) ] = (attempt, correct)
		elif row[6] == '1':
			onehop_question_result[ ( w1, w2) ] = (attempt, correct)
	csvfile.close()
	return direct_question_result, onehop_question_result
			
direct_cyc, onehop_cyc = read_results(cyc_csvfile)
direct_wn, onehop_wn = read_results(wn_csvfile)
direct_cn, onehop_cn = read_cn_results(cn_csvfile)

print 'method,w1,w2,cn_attempt,cn_correct,wn_attempt,wn_correct,cyc_attempt,cyc_correct'
for (w1, w2) in direct_cn:
	#try:
	print ','.join( map( str, [ 'direct', w1, w2, 
			direct_cn[ (w1, w2 ) ][0], direct_cn[ (w1, w2) ][1],
			direct_wn[ (w1, w2 ) ][0], direct_wn[ (w1, w2) ][1],
			direct_cyc[ (w1, w2 ) ][0], direct_cyc[ (w1, w2) ][1]
			]) )
#	except:
#		continue
for (w1, w2) in onehop_cn:
#	try:
	print ','.join( map( str, [ 'onehop', w1, w2, 
			onehop_cn[ (w1, w2 ) ][0], onehop_cn[ (w1, w2) ][1],
			onehop_wn[ (w1, w2 ) ][0], onehop_wn[ (w1, w2) ][1],
			onehop_cyc[ (w1, w2 ) ][0], onehop_cyc[ (w1, w2) ][1]
			]) )
#	except:
#		continue



