from main import SEMANTIC_NETWORK
#from model.conceptnet_api import conceptnet_api, uri_lemma
from model.semnet_api import connected_concepts, edge_lemma

from model.nlp import useful_word
from node import Node
from copy import copy
from itertools import combinations, permutations
from expand import fifo_word, random_word, highest_weight
from model.conceptnet_static import conceptnet_unspecific_relation_types, IGNORE_RELATEDTO, MAX_CONCEPTS
import string
import csv

class Subgraph:
	'''
	Take a set of words and extract all surrounding edges and nodes as a subgraph
	'''
	def __init__(self):
		self.subgraph = dict()
		self.start_words=list()
		self.total_words=0
		self.min_path_len = None
		self.unexpanded_words = list()

		self._lemmas = set()
		self._lemmas_dirty = True

		self.cf_lines = None

	def print_graph(self):
		print '####'
		for n in self.subgraph:
			print 'Node:',n, 'Start:',self.subgraph[n].start
			for r in self.subgraph[n].edges:
				print '    ',r,' : ',
				print self.subgraph[n].edges[r]
		print '####'

	def add_edge(self, source, is_source_start, target, rel_type, weight):
		if source not in self.subgraph:
			self.subgraph[source] = Node( source, is_source_start )

		if rel_type not in self.subgraph[source].edges:
			self.subgraph[source].edges[rel_type] = [ (target, weight) ]
		else:
			for (t, w) in self.subgraph[source].edges[rel_type]:
				if t==target:
					# update weight if the same
					# remove first
					self.subgraph[source].edges[rel_type].remove((t,w))
					break
			self.subgraph[source].edges[rel_type].append( (target, weight) )
		self._lemmas_dirty = True

	def remove_edge(self, source, target, rel_type, weight=None):
		if source not in self.subgraph:
			return False

		if rel_type not in self.subgraph[source].edges:
			return False
		for (t, w) in self.subgraph[source].edges[rel_type]:
			if t==target and (weight==None or w==weight):
				self.subgraph[source].edges[rel_type].remove((target, weight))
				break
		return True

	def should_stop(self, start_words):
		'''
		Test if the node expansion continues or not
		'''
		if start_words: #there are initial words unexplored still, keep going
			return False

		if self.total_words > MAX_CONCEPTS:
			return True

		return False

	def has_edge(self, start, stop, rel=None, directed=False):
		'''
		Return None if there is no edge
		Otherwise, a tuple (start, stop, relType, weight)
		If there is not edge start->stop but there is stop->start
			it will return (stop, start,...)

		>>> sg = Subgraph()
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['bogus'] = Node('bogus', False)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> sg.subgraph['apple'].edges['isA'] = [('fruit', 1.0)]
		>>> sg.subgraph['bee'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sg.subgraph['fruit'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sg.has_edge('fruit', 'flower')
		('fruit', 'flower', 'relatedTo', 1.0)
		>>> sg.has_edge('fruit', 'bee')
		(None, None, None, 0.0)
		>>> sg.has_edge('fruit', 'flower', 'relatedTo', False)
		('fruit', 'flower', 'relatedTo', 1.0)
		>>> sg.has_edge('fruit', 'flower')
		('fruit', 'flower', 'relatedTo', 1.0)
		>>> sg.has_edge('fruit', 'flower', 'relatedTo', True)
		('fruit', 'flower', 'relatedTo', 1.0)
		>>> sg.has_edge('flower', 'fruit', 'relatedTo', False)
		('fruit', 'flower', 'relatedTo', 1.0)
		>>> sg.has_edge('flower', 'fruit', 'relatedTo', True)
		(None, None, None, 0.0)
		>>> sg.has_edge('flower', 'fruit', 'isA', False)
		(None, None, None, 0.0)
		>>> sg.has_edge('flower', 'fruit', 'isA', True)
		(None, None, None, 0.0)
		'''
		# edge is specified direction
		if start in self.subgraph:
			w = start
			for r in self.subgraph[w].edges:
				for (tw, weight) in self.subgraph[w].edges[r]:
					if tw==stop and (not rel or r==rel):
						return (w, tw, r, weight)

		if not directed:
			# edge in opposite direction
			if stop in self.subgraph:
				w = stop
				for r in self.subgraph[w].edges:
					for (tw, weight) in self.subgraph[w].edges[r]:
						if tw==start and (not rel or r==rel):
							return (w, tw, r, weight)
		return (None, None, None, 0.0)

	def find_path(self, start, stop):
		'''
		Return a list of nodes from start to stop 
		[start, v1, v2..., stop]
		>>> sg=Subgraph()
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['bogus'] = Node('bogus', False)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> sg.subgraph['apple'].edges['isA'] = [('fruit', 1.0)]
		>>> sg.subgraph['bee'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sg.subgraph['fruit'].edges['relatedTo'] = [('flower', 1.0)]
		>>> print sg.find_path('apple','bee')
		['apple', 'fruit', 'flower', 'bee']
		>>> print sg.find_path('apple','bogus')
		None
		'''
		paths = self.bfs(start)
		if stop in paths:
			# return paths including the end node
			p = paths[stop]
			p.append(stop)
			return p
		else:
			return None

	def nhop_connected( self, nhop ):
		'''
		Test if there is a path of at most nhop-1 edges 
		connecting the start nodes
		'''
		p = self.all_nhop_paths( nhop, just_one=True )
		if len(p)>0:
			return True
		return False

	def all_nhop_paths( self, nhop, just_one = False ):
		paths = list()
		first = self.start_words[0]
		second = self.start_words[1]

		# try all possible paths of nhop length, order matters
		for hops in permutations( self.all_lemmas() - set( self.start_words ), nhop ):
			# check ends
			if self.has_edge( first, hops[0] ) and self.has_edge( hops[-1], second ):
				# check rest of path
				keep_path = True
				for k in range(len(hops)-1):
					if not self.has_edge( hops[k], hops[k+1] ):
						keep_path = False
						break
				if keep_path:
					paths.append( [first] + list(hops) + [second] )
					if just_one:
						return paths
		return paths

	def connected( self ):
		'''
		Test if the nodes form a connected component
		start_only - only consider the initial words, all of them need to be visited in the traversal
		rel_filter - only consider relations from this list, None if any is good

		>>> sg=Subgraph()
		>>> sg.start_words= ['apple','bee']
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> sg.subgraph['apple'].edges['isA'] = [('fruit', 1.0)]
		>>> sg.subgraph['bee'].edges['relatedTo'] = [('flower', 1.0)]
		>>> print sg.connected()
		None
		>>> sg.subgraph['fruit'].edges['relatedTo'] = [('flower', 1.0)]
		>>> print sg.connected()
		3
		'''
		subgraph = self.subgraph
		interest = self.start_words
		if len(interest)<=1:
			assert False
		paths = self.bfs(interest[0])
		for word in interest:
			if word not in paths:
				return None
		self.min_path_len = len(paths[interest[1]])
		return self.min_path_len
		
	def bfs( self, start_node ):
		'''
		in: start node
		out: dictionary of paths
		>>> sg=Subgraph()
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> print sg.bfs('apple')
		{'apple': []}
		>>> sg.subgraph['apple'].edges['isA'] = [('fruit', 1.0)]
		>>> sg.subgraph['bee'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sg.subgraph['fruit'].edges['relatedTo'] = [('flower', 1.0)]
		>>> paths = sg.bfs('apple')
		>>> print paths
		{'fruit': ['apple'], 'apple': [], 'flower': ['apple', 'fruit'], 'bee': ['apple', 'fruit', 'flower']}
		'''
		q = list()
		v = set()
		paths = dict()

		q.append( start_node ) # add at end
		v.add( start_node )
		paths[start_node] = list()
		while q:
			t = q.pop(0) # remove first from queue
			# add out edges first, if any
			if t in self.subgraph:
				for r in self.subgraph[t].edges:
					for (u, weight) in self.subgraph[t].edges[r]:
						if u not in v:
							v.add(u)
							q.append(u)
							#if t not in paths:
							#	paths[t] = list()
							if u not in paths:
								paths[u]=paths[t]+[t]
			# now add in edges
			for n in self.subgraph:
				for r in self.subgraph[n].edges:
					for (u, weight) in self.subgraph[n].edges[r]:
						if u == t and n not in v:
							v.add(n)
							q.append(n)
							paths[n] = paths[u]+[u]
		return paths 

	def all_lemmas( self ):
		'''
		Return a set of all known lemmas in the subgraph

		>>> sg=Subgraph()
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> sg.subgraph['apple'].edges['isA'] = [('fruit', 1.0)]
		>>> sg.subgraph['fruit'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sg.subgraph['bee'].edges['relatedTo'] = [('flower', 1.0)]
		>>> sorted(list(sg.all_lemmas()))
		['apple', 'bee', 'flower', 'fruit']
		'''
		# use pre-computed lemmas for efficiency, this goes dirty every time a new word is added
		if not self._lemmas_dirty:
			return self._lemmas

		subgraph = self.subgraph
		result = set()
		for n in subgraph.values():
			result.add(n.name)
			for l in n.edges.values():
				for (word, weight) in l:
					result.add(word)

		self._lemmas = result
		self._lemmas_dirty = False
		return result

	def choose_node_to_expand( self ):
#		return highest_weight( self )
#		return random_word( self )
		return fifo_word( self )

	def create_subgraph( self, start_words ):
		'''
		Start with a list of lemmas
		Expand each into a node
		After, choose which edge to expand
		Continue until stopping condition is met

		Only add Node objects for out edges
		If a concept (lemma) only has in edges,
			it will only be found in the list of another Node
			under the respective relation types

		>>> sn = Subgraph()
		>>> sn.create_subgraph( [u'goose', u'flock'] )
		>>> print sn.start_words
		[u'goose', u'flock']
		>>> assert sn.connected() is not None
		'''
		self.start_words = copy(start_words)
		self.total_words = 0
		while True:
			# check stopping condition
			if self.should_stop(self.start_words):
				self.start_words = copy(start_words)
				return

			is_start = False
			if len( self.start_words ) > 0:
				node_name = self.start_words.pop(0)
				is_start = True
			else:	
				node_name = self.choose_node_to_expand()
				if node_name == None:
					#print '!!!!!! expand node NULL', node_name
					self.start_words = copy(start_words)
					return

			if node_name not in self.subgraph:
				self.subgraph[node_name] = Node( node_name, is_start )
				self._lemmas_dirty = True
			node = self.subgraph[node_name]

			#update start value
			#if the node was added as edge end before
			if is_start:
				node.start=is_start 

			#concept_json_edges = conceptnet_api(node_name)
			concept_json_edges = connected_concepts(node_name, SEMANTIC_NETWORK)

			if concept_json_edges == None:
				print 'Nothing fount in ConceptNet for ', node_name

			for e in concept_json_edges:
				if e is None:
					continue	
				rel_type = e.get(u'rel')
				weight = e.get(u'weight')

				if not rel_type or not weight:
					continue
				if rel_type == u'/r/TranslationOf': #only English for now
					continue
				if IGNORE_RELATEDTO and rel_type in conceptnet_unspecific_relation_types:
					continue

				#startlemmas = uri_lemma( e.get(u'start') ) 
				#endlemmas = uri_lemma( e.get(u'end') ) 
				startlemmas = edge_lemma( e.get(u'start'), SEMANTIC_NETWORK ) 
				endlemmas = edge_lemma( e.get(u'end'), SEMANTIC_NETWORK ) 
				if (startlemmas is None) or (endlemmas is None) \
					or not( useful_word(startlemmas) and useful_word(endlemmas) ) \
					or startlemmas==endlemmas:
					continue

				# outward edge node --- rel ---> other_node
				if node_name == startlemmas:
					#if node_name!=startlemmas:
					#	print 'Nonmatch ',node_name, startlemmas
					other_node_name = endlemmas
					# add it here as node
					if other_node_name not in self.subgraph:
						other_node = Node(other_node_name, False)
						self.subgraph[other_node_name] = other_node
				
					if rel_type not in node.edges:
						node.edges[rel_type] = list()

					node.edges[rel_type].append( (other_node_name, weight) )

					node.expanded = True
					self.unexpanded_words.append(other_node_name)
					self.total_words += 1

					# check if the new word is connected 
					# to any other words in the graph
					self.check_new_node_edges( other_node_name )

				# inward edge other_node --- rel ---> node	
				elif node_name in endlemmas: 
					#if node_name!=endlemmas:
					#	print 'Nonmatch ',node_name, endlemmas
					other_node_name = startlemmas

					if other_node_name not in self.subgraph:
						other_node = Node(other_node_name, False)
						self.subgraph[other_node_name] = other_node
					else:
						other_node = self.subgraph[other_node_name]

					if rel_type not in other_node.edges:
						other_node.edges[rel_type] = list()
					if rel_type not in node.edges:
						node.edges[rel_type]=list()

					other_node.edges[rel_type].append( (node_name, weight) )

					node.expanded = True
					self.unexpanded_words.append(other_node_name)
					self.total_words += 1

					# check if the new word is connected 
					# to any other words in the graph
					self.check_new_node_edges( other_node_name )
		# final cleaning
		self.clean_subgraph()
		self._lemmas_dirty = True

	def clean_subgraph(self):
		'''
		Remove duplicate edges, orphan nodes
		'''
		orphans = list()
		for n in self.subgraph:
			if len(self.subgraph[n].edges)==0:
				# may be an orphan, check for in edges later
				orphans.append(n)
			else:
				for rel in self.subgraph[n].edges:
					cleaned = list()
					for m,w in self.subgraph[n].edges[rel]:
						same_word = list()
						# there is an in edge, not an orphan
						if m in orphans:
							orphans.remove(m)

						# go again through all edges for this relation
						# and record for all matches on m
						for x,y in self.subgraph[n].edges[rel]:
							if x==m:
								same_word.append( (x,y) )
						assert len(same_word)>0, True
						# get the max weight
						max_weight = same_word[0][1]
						for x,y in same_word:
							if y>max_weight:
								max_weight = y
						# save only the max weight
						cleaned.append( (m, max_weight) )
					# replace the list of edges only with the saved max per word
					# i.e. remove all duplicates
					assert len(cleaned)<=len(self.subgraph[n].edges[rel]), True
					assert len(cleaned)>0, True
					self.subgraph[n].edges[rel] = cleaned
		for x in orphans:
			if x in self.subgraph and not self.subgraph[x].start:
				self.subgraph.pop(x)
		self._lemmas_dirty = True

	def check_new_node_edges( self, new_node_name ):
		new_node = self.subgraph[new_node_name]
		current_lemmas = self.all_lemmas()
		
		#concept_json_edges = conceptnet_api(new_node_name)
		concept_json_edges = connected_concepts( new_node_name, SEMANTIC_NETWORK )
		if concept_json_edges is None:
			print 'NONE JSON', new_node_name
			return # TODO this is not ok, but don't fail yet

		for e in concept_json_edges:
			if not e:
				break

			rel_type = e.get(u'rel')
			weight = e.get(u'weight')

			if not rel_type or not weight:
				continue
			if rel_type == u'/r/TranslationOf': #only English for now
				continue
			if IGNORE_RELATEDTO and rel_type in conceptnet_unspecific_relation_types:
				continue

			#startlemmas = uri_lemma (e.get(u'start'))
			#endlemmas = uri_lemma (e.get(u'end'))
			startlemmas = edge_lemma( e.get(u'start'), SEMANTIC_NETWORK ) 
			endlemmas = edge_lemma( e.get(u'end'), SEMANTIC_NETWORK ) 
			if (startlemmas is None) or (endlemmas is None)  \
				or not( useful_word(startlemmas) and useful_word(endlemmas) ):
				continue

			# outward edge node --- rel ---> other_node
			if new_node_name in startlemmas:
				other_node_name = endlemmas
	
				# only check for existing nodes, don't add anything new here
				if other_node_name not in current_lemmas:
					continue

				if rel_type not in new_node.edges:
					new_node.edges[rel_type] = list()
				# don't add same link twice
				if new_node.edges[rel_type] and other_node_name not in new_node.edges[rel_type][:][0]:
					new_node.edges[rel_type].append( (other_node_name, weight) )

			# inward edge other_node --- rel ---> node	
			elif new_node_name in endlemmas: 
				other_node_name = startlemmas

				# only check for existing nodes, don't add anything new here
				if other_node_name not in current_lemmas:
					continue

				if other_node_name not in self.subgraph:
					assert True, False # error, should be in the graph
				else:
					other_node = self.subgraph[other_node_name]

				if rel_type not in other_node.edges:
					other_node.edges[rel_type] = list()
				# don't add same link twice
				if other_node.edges[rel_type] and new_node_name not in other_node.edges[rel_type][:][0]:
					other_node.edges[rel_type].append( (new_node_name, weight) )

	def connectivity_graph( self ):
		'''
		return a graph that for every pair of nodes
		if there is one or more oriented edges all in one direction between them
		it will have exactly one edge
		i.e. it reflects the connectiviy of the graph
		>>> from copy import copy
		>>> sg=Subgraph()
		>>> sg.subgraph['apple'] = Node('apple', True)
		>>> sg.subgraph['bee'] = Node('bee', True)
		>>> sg.subgraph['fruit'] = Node('fruit', False)
		>>> sg.subgraph['apple'].edges[u'/r/IsA'] = [('fruit', 1.0)]
		>>> sg.subgraph['apple'].edges[u'/r/RelatedTo'] = [('fruit', 1.0)]
		>>> sg.subgraph['apple'].edges[u'/r/NotIsA'] = [('flower', 1.0)]
		>>> sg.subgraph['fruit'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
		>>> sg.subgraph['fruit'].edges[u'/r/RelatedTo'] = [('apple', 1.0)]
		>>> sg.subgraph['bee'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
		>>> cg = copy(sg)
		>>> cg.subgraph = sg.connectivity_graph()
		>>> cg.print_graph()
		####
                Node: fruit Start: False
                     edge  :  [('apple', 1.0)]
                Node: apple Start: True
                     edge  :  [('fruit', 1.0), ('flower', 1.0)]
                Node: bee Start: True
                     edge  :  [('flower', 1.0)]
		####
		'''
		connectivity_graph = dict()
		for (a,b) in permutations(self.all_lemmas(), 2):
			start, stop, rel, weight = self.has_edge(a,b, directed=True)
			if start == None:
				continue
			if a not in connectivity_graph:
				connectivity_graph[a] = Node( a, self.subgraph[a].start )
				connectivity_graph[a].edges['edge'] = []
			if b not in connectivity_graph[a].edges['edge']:
				connectivity_graph[a].edges['edge'].append( (b,1.0) )
		return connectivity_graph


if __name__ == '__main__':
	import doctest
	doctest.testmod()

