from sse.compare import compare_direct_paths_maxweight

#from model.conceptnet_api import conceptnet_edges_between
from model.semnet_api import edges_between_concepts

#from model.conceptnet_static import conceptnet_relation_english_names
from model.semnet_api import relation_english_names

from main import SEMANTIC_NETWORK

def explain_solution( path_pair ):
	'''
	Input two node sequences
	Reconstruct paths from CN
	Select salient edges per segment
	Replace edge names with text
	Return strings
	'''

	p1, p2 = path_pair

	p1 = p1.encode('ascii', 'ignore').split(' ')
	p2 = p2.encode('ascii', 'ignore').split(' ')

	assert len(p1) == len(p2)

	p1 = reconstruct_path( p1 )
	p2 = reconstruct_path( p2 )

	return explain_path_pair( p1, p2 )

def reconstruct_path( path ):
	'''
	Input a list of concepts
	Return full path
	'''
	result = list()
	for k in range( len(path) - 1 ):
		# append node
		result.append( path[k] )

		# OLD edges = conceptnet_edges_between( path[k], path[k+1] )
		edges = edges_between_concepts( path[k], path[k+1], SEMANTIC_NETWORK )

		assert edges is not None, 'Broken path' + path[k] + ' ' + path[k+1]
		result.append( edges )
	# add the remainder of the path
	assert type(path[-1]) == str, str(path)
	result.append( path[-1] )
	return result

def explain_path_pair( p1, p2 ):
	'''
	Iterate segments and select salient edges
	''' 
	expl1 = list() 
	expl2 = list()
	for k in range( len(p1) ):
		if type(p1[k]) == str:
			#add concepts
			assert type(p2[k])==str
			expl1.append( p1[k] )
			expl2.append( p2[k] )
		elif type(p1[k]) == list:
			assert type(p2[k])==list
			# select and add best edge
			#best_edge = salient_edge( p1[k], p2[k] )
			#expl1.append(best_edge)
			#expl2.append(best_edge)
			
			# add all common edges
			ce = common_edges( p1[k], p2[k] )
			for edge in ce:
				expl1.append(edge)
				expl2.append(edge)
				
	return expl1, expl2

def salient_edge( edges1, edges2 ):
	'''
	Minimum support algorithm from analogy paper 
	Here it select directly from edge sets
	'''
	best_edge = None
	best_weight = None
	for e1, w1 in edges1:
		for e2, w2 in edges2:
			if e2 == e1 and e1!=u'/r/RelatedTo':
				if min(w1, w2)>0 and min(w1, w2) > best_weight:
					best_edge = e1
					best_weight = min(w1,w2)
				
	return str(best_edge), str(best_weight)

def common_edges( edges1, edges2 ):
	'''
	Minimum support algorithm from analogy paper 
	Here it select directly from edge sets
	'''
	common_edges = list()
	for e1, w1 in edges1:
		for e2, w2 in edges2:
			if e2 == e1 and e1!=u'/r/RelatedTo':
				edge = e1
				weight = min(w1,w2)
				common_edges.append( (edge, weight) )
	return common_edges
				

def explain_in_english( expl, english_translate=True ):
	'''
	Use templates for edges to convert to a pseudo-sentence
	'''

	return ' '.join( map( str, expl ) )
	
def explain_to_string( path_pair, english=False ):
	'''
	Combine explain_solution and explain_in_english in a single call and return a string with both
	'''
	expl = explain_solution( map( ' '.join, path_pair ))
        if expl is not None and type(expl)==tuple:
		expl1, expl2 = expl
		if english:
			expl1 = explain_in_english( expl1 )
			expl2 = explain_in_english( expl2 )
		else:
			expl1 = explain_in_english( expl1, False )
			expl2 = explain_in_english( expl2, False )
		return expl1 + ';' + expl2
	else:
		return ''
