from sse.subgraph import Subgraph
from sse.node import Node
from sse.prune_graph import prune_non_paths
from itertools import permutations, combinations
from model.conceptnet_static import conceptnet_all_relation_types
from model.conceptnet_static import conceptnet_inheritable_relation_types
from model.conceptnet_static import conceptnet_unspecific_relation_types
from model.conceptnet_static import IGNORE_RELATEDTO

all_relations = conceptnet_all_relation_types

def compare_direct_paths_jaccard( p1, snp1, p2, snp2 ):
        # gather all rel types between the nodes
        edges1 = dict()
        edges2 = dict()
        for (first, second, snp, edges) in [ (p1[0], p1[1], snp1, edges1), (p2[0], p2[1], snp2, edges2) ]:
                for rel in all_relations:
			if IGNORE_RELATEDTO and rel in conceptnet_unspecific_relation_types:
				continue
                        start, stop, r, weight = snp.has_edge(first, second, rel=rel, directed=True)
                        if start!=None:
				edges[ rel ] = weight
                        start, stop, r, weight = snp.has_edge(first, second, rel=rel, directed=True)
                        if start!=None:
				edges[ rel ] = weight

        # compute intersection and union of rel type sets
        intersection = set()
        union = set()
        intersection = set(edges1.keys()).intersection(set(edges2.keys()))
        union = set(edges1.keys()).union(set(edges2.keys()))

        if len(union)>0:
                jaccard = float( len( intersection )) / len( union )
        else:
                jaccard = 0.0

	# get best common relation for explanation
	bufweight = 0.0
	bestrel = None
	for reltype in edges1:
		if reltype in edges2:
			if bufweight < min(edges1[reltype], edges2[reltype]):
				bufweight = min(edges1[reltype], edges2[reltype])
				bestrel = reltype

        return jaccard, bestrel

def compare_direct_paths_maxweight( p1, snp1, p2, snp2):
        # gather all rel types between the nodes
        edges1 = dict()
        edges2 = dict()
        for (first, second, snp, edges) in [ (p1[0], p1[1], snp1, edges1), (p2[0], p2[1], snp2, edges2) ]:
                for rel in all_relations:
			if IGNORE_RELATEDTO and rel in conceptnet_unspecific_relation_types:
				continue
                        start, stop, r, weight = snp.has_edge(first, second, rel=rel, directed=True)
                        if start!=None:
                                edges[ rel+"_fwd" ] = weight
                        start, stop, r, weight = snp.has_edge(first, second, rel=rel, directed=True)
                        if start!=None:
                                edges[ rel+"_back" ] = weight

	# find the common edge that has the highest minimum weight between the two graphs
	# that means that on both sides, it has at least that level of support
	# avoiding misleading numbers such as averages
	# the orientation is encoded in the key, so it has to match
	bufweight = 0.0
	bestrel = None
	for reltype in edges1:
		if reltype in edges2:
			if bufweight < min(edges1[reltype], edges2[reltype]):
				bufweight = min(edges1[reltype], edges2[reltype])
				bestrel = reltype
			
	return bufweight, bestrel 

def compare_direct_paths( p1, snp1, p2, snp2 ):
	return compare_direct_paths_jaccard( p1, snp1, p2, snp2 )

# extension of compare one hop
def compare_nhop_paths( p1, snp1, p2, snp2 ):
        assert len(p1)==len(p2), True
        mark = 1.0
        nhop = len(p1)-1
        for k in range(nhop):
                markseg, bestrel = compare_direct_paths( p1[k:k+2], snp1, p2[k:k+2], snp2 )
                mark *= markseg
        return mark

def find_contradiction_direct( p1, snp1, p2, snp2 ):
        '''
        For two direct paths, check if there is a collapsible edge
        That has different directions in the two graphs
        Collapsible edges indicate class-superclass relations
        so if there is a contradiction, the analogy makes no sense
        '''
        return False
        # only a smaller subset of edges cause contradictions
        # synonims, for example, shouldn't, because they are symmetric, derivative is too vauge
        for rel in conceptnet_inheritable_relation_types:
                        start1, stop1, r1, weight1 = snp1.has_edge(p1[0], p1[1], rel=rel, directed=True)
                        start1b, stop1b, r1b, weight1b = snp1.has_edge(p1[1], p1[0], rel=rel, directed=True)

                        start2, stop2, r2, weight2 = snp2.has_edge(p2[0], p2[1], rel=rel, directed=True)
                        start2b, stop2b, r2b, weight2b = snp2.has_edge(p2[1], p2[0], rel=rel, directed=True)

                        # a ---> b , c <--- d
                        if start1 and start2b:
                                return True
                        if start2 and start1b:
                                return True

        # no contradiction found
        return False

def find_contradiction_nhop( p1, snp1, p2, snp2, nhop ):
        return False
        assert len(p1)==len(p2), True
        nhop = len(p1)-1
        for k in range(nhop):
                if find_contradiction_direct( p1[k:k+2], snp1, p2[k:k+2], snp2 ):
                        return True
        return False

