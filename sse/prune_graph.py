from itertools import combinations, permutations
from copy import deepcopy
from sse.subgraph import Subgraph
from sse.node import Node

def prune_non_paths( sg ):
	'''
	>>> sn = Subgraph( )
	>>> sn.create_subgraph( ('bird', 'duck' ) ) 
	>>> snp = prune_non_paths( sn )
	>>> print snp.connected()
	1
	>>> sn = Subgraph( )
	>>> sn.create_subgraph( ('goose', 'flock' ) ) 
	>>> snp = prune_non_paths( sn )
	>>> print snp.connected()
	2
	'''
	buf = deepcopy(sg)
	out = Subgraph()
	out.start_words = deepcopy(sg.start_words)
	#if len(out.start_words)<2:
	#	print 'START WORDS', sg.start_words, out.start_words
	start = sg.start_words[0]
	stop = sg.start_words[1]

	# get bfs
	# add all paths reaching stop, mark all these nodes
	# for all others starting in start, check if the end has a connection to a marked node
	# if so, keep these as well
	paths = buf.bfs( start )
	back_paths = buf.bfs( stop )
	paths_recently_added = list()
	keep_nodes = set()
	# all paths begin with start, don't need to check that
	for end_node in paths: # paths is a dictionary (node -> path to node), use the path
		p = paths[end_node]
		p.append(end_node)
		keep_path = False

		assert p[0]==start, True
		# if the path passes through stop, strip everything after stop and save it
		if stop in p:
			while p[-1]!=stop:
				p.pop()
			keep_path = True
		else:
			continue # check the others later

		if keep_path:
			keep_nodes = keep_nodes.union( set( p ) )

		paths_recently_added.append(end_node)

	if len(paths_recently_added)==0:
		return None
	for n in paths_recently_added:
		paths.pop(n)

	# add all edges between all possible pairs of nodes to be kept
	for ( x, y ) in combinations( keep_nodes, 2 ):
		# same as in the old code, add one rel type to out
		# and remove it from buf, 
		# until there are no more edges in buf between x and y
		while True:
			source, target, rel_type, weight = buf.has_edge(x,y)
			if not source:
				break
			# add edge to out
			out.add_edge( source, buf.subgraph[source].start, target, rel_type, weight)
			# remove edge from buf, this HAS to work
			success = buf.remove_edge( source, target, rel_type, weight)
			assert success, True

	# once the most direct paths to stop have been added 
	# check the others flowing into them until all are added
	for end_node in paths:
		p=paths[end_node]
		keep_path = False

		assert p[0]==start, True
		# the part of p that is already marked must be contiguous and at the beginning
		for i in range(len(p)):
			if p[i] in keep_nodes:
				continue
			else:
				# there are nodes left in p that haven't been added
				if len(p[i:])>0 and len(keep_nodes.intersection(p[i:]))==0:
				# in this case, find a way to get to stop
				# that doesn't intersect with the path 
					if (end_node in back_paths) and (len(set(back_paths[end_node]).intersection(p))==0):
						p = p + back_paths[end_node]
						keep_path = True
		if keep_path:
			keep_nodes = keep_nodes.union( set( p ) )

	# add all edges between all possible pairs of nodes to be kept
	for ( x, y ) in combinations( keep_nodes, 2 ):
		# same as in the old code, add one rel type to out
		# and remove it from buf, 
		# until there are no more edges in buf between x and y
		while True:
			source, target, rel_type, weight = buf.has_edge(x,y)
			if not source:
				break
			# add edge to out
			out.add_edge( source, buf.subgraph[source].start, target, rel_type, weight)
			# remove edge from buf
			success = buf.remove_edge( source, target, rel_type, weight)
			assert success, True

	# make sure that all start words have their nodes
	# even if they are not sources for any edge
	out._lemmas_dirty = True
	for w in sg.start_words:
		if w not in out.subgraph:
			out.subgraph[w] = Node(w,True)
			out._lemmas_dirty = True

	return out

def merge_nodes( sg, start=None, stop=None ):
	'''
	Merge a pair of nodes and return a new graph with the change
	The new node has the name of stop, because the merging means that 
		start is abstracted to stop
	For example if "dog ---isA---> canine" is merged,
	dog is replaced by its superclass, canine

	>>> sg = Subgraph()
	>>> sg.start_words = [ 'apple','bee' ]
	>>> sg.subgraph['apple'] = Node('apple', True)
        >>> sg.subgraph['bee'] = Node('bee', True)
        >>> sg.subgraph['fruit'] = Node('fruit', False)
        >>> sg.subgraph['food'] = Node('food', False)
        >>> sg.subgraph['apple'].edges[u'/r/IsA'] = [('fruit', 1.0)]
        >>> sg.subgraph['apple'].edges[u'/r/NotIsA'] = [('flower', 1.0)]
	>>> sg.subgraph['fruit'].edges[u'/r/IsA'] = [('food', 1.0)]
        >>> sg.subgraph['bee'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
        >>> sg.subgraph['fruit'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
	>>> msg = merge_nodes( sg, 'apple', 'bee' ) 
	>>> print msg
	None
	>>> msg = merge_nodes( sg, 'fruit', 'food' )
	>>> msg.print_graph()
	####
	Node: food Start: False
	     /r/RelatedTo  :  [('flower', 1.0)]
	Node: apple Start: True
	     /r/IsA  :  [('food', 1.0)]
	     /r/NotIsA  :  [('flower', 1.0)]
	Node: bee Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	####
	>>> msg = merge_nodes( sg, 'apple', 'fruit' )
	>>> msg.print_graph()
	####
	Node: food Start: False
	Node: apple Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	     /r/IsA  :  [('food', 1.0)]
	     /r/NotIsA  :  [('flower', 1.0)]
	Node: bee Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	####
	>>> msg = merge_nodes( sg, 'apple', 'flower' )
	>>> msg.print_graph()
	####
	Node: food Start: False
	Node: fruit Start: False
	     /r/RelatedTo  :  [('flower', 1.0)]
	     /r/IsA  :  [('food', 1.0)]
	Node: apple Start: True
	     /r/IsA  :  [('fruit', 1.0)]
	Node: bee Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	####
	'''

	# if there is no edge, can't merge
	if sg.has_edge( start, stop )[0] is None:
		return None
	# nonsense
	if start==stop:
		return None

	# do not merge the start words
	if (start in sg.subgraph) and sg.subgraph[start].start \
		and (stop in sg.subgraph) and sg.subgraph[stop].start:
		return None

	out = deepcopy(sg)

	# check if stop has its own entry in the subgrap
	# if not (no out edges), create it 
	if stop not in out.subgraph:
		# default to False, if it has no node yet, it is not a start word
		out.subgraph[stop] = Node( stop, False )

	# if set, copy the start flag over to stop
	# we will rename later to start, this keeps all other code the same for both start nodes and regular ones
	if out.subgraph[start].start:
		out.subgraph[stop].start = True

	# copy all start's relations to stop
	# out edges first
	for rel in out.subgraph[start].edges:
		if rel not in out.subgraph[stop].edges:
			out.subgraph[stop].edges[rel] = list()

		# copy one by one, 
		# checking for duplicates and selecting the max weight if this happens
		to_remove = set()
		to_append = set()
		for n1,w1 in out.subgraph[start].edges[rel]:
			# stop will be merged, no edges to copy
			# also, don't copy edges pointing from start to itself
			if n1==stop or n1==start:
				continue
			# TODO this doesn't copy the max weight, but it is not an issue right now 
			# since weights are not used anywhere
			duplicate = False
			for n2,w2 in out.subgraph[stop].edges[rel]:
				if n1==n2:
				# found a node that both start and stop point to
					duplicate = True
					break
			if not duplicate:
				out.subgraph[stop].edges[rel].append( (n1, w1) )
	
	# in edges, for which just the name has to be changed in the list, from start into stop
	for n in out.subgraph:
		for rel in out.subgraph[n].edges:
			for i in range(len(out.subgraph[n].edges[rel])):
				# if start, only copy edge if it is not from stop
				x, w = out.subgraph[n].edges[rel][i]
				if x == start and n!=stop:
					out.subgraph[n].edges[rel][i] = (stop, w)
	# clean empty edge lists
	for n in out.subgraph:
		remove_rels = list()
		for rel in out.subgraph[n].edges:
			if len(out.subgraph[n].edges[rel]) == 0:
				remove_rels.append(rel)
		for rel in remove_rels:
			out.subgraph[n].edges.pop(rel)
						
	# remove start from the subgraph
	out.subgraph.pop(start)

	# final check if start is not in the graph
	for n in out.subgraph:
		assert n!=start, True
		for rel in out.subgraph[n].edges:
			for m,w in out.subgraph[n].edges[rel]:
				if m==start:
					assert True, False

	# if we merged a node with node.start == True, rename the destination to that node
	# so that it still matches in the problem when this function returns
	if out.subgraph[stop].start == True:
		out.subgraph[start] = out.subgraph[stop]
		out.subgraph.pop(stop)

	return out

def get_candidate_edges(sg):
	'''
	Find mergeable edges and return them as a list
	>>> sg = Subgraph()
	>>> sg.start_words = [ 'apple','bee' ]
	>>> sg.subgraph['apple'] = Node('apple', True)
        >>> sg.subgraph['bee'] = Node('bee', True)
        >>> sg.subgraph['fruit'] = Node('fruit', False)
        >>> sg.subgraph['food'] = Node('food', False)
        >>> sg.subgraph['apple'].edges[u'/r/IsA'] = [('fruit', 1.0)]
	>>> sg.subgraph['fruit'].edges[u'/r/IsA'] = [('food', 1.0)]
        >>> sg.subgraph['bee'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
        >>> sg.subgraph['fruit'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
	>>> print get_candidate_edges(sg)
	[(u'/r/IsA', 'apple', 'fruit'), (u'/r/IsA', 'fruit', 'food')]
	'''
	conceptnet_mergeable_relation_types = [] # TODO 
	candidate_edges = set() #tuple list, (rel, node1, node2)
				# where rel is a mergeable relation from node1 to node2
	# for each mergeable edge, save the edge as a candidate
	for n in sg.subgraph:
		for merge_rel in conceptnet_mergeable_relation_types:
			if merge_rel in sg.subgraph[n].edges:
				for m,w in sg.subgraph[n].edges[merge_rel]:
					candidate_edges.add( (merge_rel, n, m) )

	# remove from the candidate edge set those relations linking start nodes
	# because two start nodes are needed for an analogy
	rejected_candidates = set()
	for e in candidate_edges:
		if e[1] in sg.subgraph and e[2] in sg.subgraph and \
			sg.subgraph[ e[1] ].start and sg.subgraph[ e[2] ].start:
			rejected_candidates.add(e)
	candidate_edges = candidate_edges.difference( rejected_candidates )

	return list(candidate_edges)

def merge_full( sg ):
	'''
	Merge everything in a graph and return a single answer, 
	not all possible merges like merge_space_full

	>>> sg = Subgraph()
	>>> sg.start_words = [ 'apple','bee' ]
	>>> sg.subgraph['apple'] = Node('apple', True)
        >>> sg.subgraph['bee'] = Node('bee', True)
        >>> sg.subgraph['fruit'] = Node('fruit', False)
        >>> sg.subgraph['food'] = Node('food', False)
        >>> sg.subgraph['apple'].edges[u'/r/IsA'] = [('fruit', 1.0)]
        >>> sg.subgraph['apple'].edges[u'/r/NotIsA'] = [('flower', 1.0)]
	>>> sg.subgraph['fruit'].edges[u'/r/IsA'] = [('food', 1.0)]
        >>> sg.subgraph['bee'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
        >>> sg.subgraph['fruit'].edges[u'/r/RelatedTo'] = [('flower', 1.0)]
	>>> msg = merge_full( sg )
	>>> msg.print_graph()
	####
	Node: apple Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	     /r/NotIsA  :  [('flower', 1.0)]
	Node: bee Start: True
	     /r/RelatedTo  :  [('flower', 1.0)]
	####
	'''
	out = sg
	# recompute the candidate edges after every node merge
	# because some nodes dissapear and produce conflicts
	candidate_edges = get_candidate_edges(sg)
	while len(candidate_edges)>0:
		edge = candidate_edges.pop(0)
		buf = merge_nodes( out, edge[1], edge[2] )
		if not buf:
			continue # could not merge, for example it tried merging the start node
		else:
			out = buf
			candidate_edges = get_candidate_edges(out)
	return out

