import urllib, urllib2, json

class Node:
	'''
	name - conceptnet node name (word or phrase)
	
	edges - has relation names as keys and node lists as values
	the nodes in each list are concept names

	start - one of the initial words

	'''
	def __init__(self, name, start):
		self.name = name
		self.edges = dict()
		self.start = start
		self.expanded = False


if __name__=='__main__':
	import doctest
	doctest.testmod()
