import random

def highest_weight( sg ):
	'''
	Select the edge with the highest weight
	'''
	max_weight = 0.0
	max_word = None
	for w in sg.subgraph:
		for r in sg.subgraph[w].edges:
			for x, weight in sg.subgraph[w].edges[r]:
				# if the node w exists as a souce, but has not been expanded
				# in this case, w is in its edges and
				# the node was added when w was expanded
				if x in sg.subgraph and not sg.subgraph[x].expanded:
					for r in sg.subgraph[x].edges:
						for ww, wweight in sg.subgraph[x].edges[r]:
							# the edge that has w as target
							if ww == w and max_weight < wweight:
								max_weight = wweight
								max_word = x
			
				# if the node is only as target (i.e. not expanded)	
				elif x not in sg.subgraph:
					if max_weight < weight:
						max_weight = weight
						max_word = x
	return max_word

def random_word( sg ):
	while True:
		w = random.choice( sg.unexpanded_words )
		sg.unexpanded_words.remove( w )
		if w in sg.subgraph and sg.subgraph[w].expanded==True:
			pass
		else:
			return w
	return None 

def fifo_word( sg ):
	while sg.unexpanded_words:
		w = sg.unexpanded_words.pop(0)
		if w in sg.subgraph and sg.subgraph[w].expanded==True:
			pass
		else:
			return w
	return None
