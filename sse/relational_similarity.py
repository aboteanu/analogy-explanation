from sse.subgraph import Subgraph
from sse.prune_graph import prune_non_paths, merge_full
from model.worker_thread import WorkerThread
from model.conceptnet_static import conceptnet_all_relation_types, conceptnet_inheritable_relation_types
import random
from sse.compare import compare_direct_paths, compare_nhop_paths, find_contradiction_direct, find_contradiction_nhop

def relational_similarity( snp1, snp2 ):
	'''
	Interfaces with the analogy to return the best path pair and score
	
	input: two subgraphs 
	output: best explanation path and path score

	'''

	best_path = None
	score = 0.0

	# compare all direct paths
	direct_mark, best_direct_path = best_path_nhop( snp1, snp2, 0 )

	# compare one hop paths
	onehop_mark, best_onehop_path = best_path_nhop( snp1, snp2, 1 )

	# return both one hop and direct
	return [(best_direct_path, direct_mark) , (best_onehop_path, onehop_mark) ]

def best_path_nhop( snps1, snps2, nhop ):

	mark = None
	solution_path = None

	if not snps1 or not snps2:
		return None, None

	if nhop==0:
		if (not snps1.has_edge(snps1.start_words[0], snps1.start_words[1])) or \
			(not snps2.has_edge(snps2.start_words[0], snps2.start_words[1])):
			return None, None

		mark, solution_path = compare_direct_paths( snps1.start_words, snps1, snps2.start_words, snps2)
		return mark, solution_path
				
	elif nhop>=1:
		if (not snps1.nhop_connected(nhop)) or (not snps1.nhop_connected(nhop)):
			return None, None

		nhop_1 = snps1.all_nhop_paths(nhop)
		nhop_2 = snps2.all_nhop_paths(nhop)

		for p1 in nhop_1:
			for p2 in nhop_2:
				mark_pa = compare_nhop_paths( p1, snps1, p2, snps2 )
				if mark_pa > mark:
					mark = mark_pa
					solution_path = (p1, p2)
		return mark, solution_path
	else:
		return None, None
