        
if __name__=="__main__":
	from model.sat_question_reader import SATReader
	from model.levels_question_reader import LevelsReader

	analogyfile = open('analogyfile.txt', 'wt')
        testanalogies = open('testanalogies.txt', 'wt')
        for level in range(1,12) + ['sat']:
                if level == 'sat':
                        qreader = SATReader()
                else:
                        qreader = LevelsReader(level)

		for q in qreader:
			if not q:
				break
			for p in [q.question]+q.answers:
				analogyfile.write( ':'.join( p.split(',') ) + '\n' )

			for a in q.answers:
				testanalogies.write( ':'.join( q.question.split(',') ) 
						+ '::' 
						+ ':'.join( a.split(',') ) 
						+'\n' )

        analogyfile.close()
        testanalogies.close()

