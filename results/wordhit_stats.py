import csv

csvfile = open( 'semnet_qwords_hit.csv', 'rb' ) 
reader = csv.reader( csvfile, delimiter=',' )
hit_result = dict()
miss_result = dict()
for row in reader:
	level = row[0]
	net = row[1]
	hit = row[2]
	if level == "level":
		continue
	
	if (level,net) not in hit_result:
		hit_result[ (level, net) ] = 0
	if (level,net) not in miss_result:
		miss_result[ (level, net) ] = 0

	if hit == "1":
		hit_result[ (level, net) ] += 1
	elif hit == "0":
		miss_result[ (level, net) ] +=1

csvfile.close()

print 'level,net,hit,total'
for (level, net) in hit_result:
	print ','.join( map( str, [level,net,hit_result[(level,net)], miss_result[(level,net)]+hit_result[(level,net)]]) )
