import csv

csvfile = open( 'answers_cyc.csv', 'rb' ) 
reader = csv.reader( csvfile, delimiter=',' )
attempts = dict()
correct = dict()
total = dict()
for row in reader:
	level = row[0]
	net_method = row[3]
	score = row[4]	
	answer = row[5]
	solution= row[6]

	if level == "level":
		continue
	
	if (level,net_method) not in attempts:
		attempts[ (level, net_method) ] = 0
	if (level,net_method) not in correct:
		correct[ (level, net_method) ] = 0
	if (level,net_method) not in total:
		total[ (level, net_method) ] = 0

	total[ (level, net_method) ] += 1
	if answer != "None":
		attempts[ (level, net_method) ] += 1
	if answer != "None" and answer==solution:
		correct[ (level, net_method) ] +=1

csvfile.close()

print 'level,net,total,attempt,correct'
for (level, net) in total:
	print ','.join( map( str, [level,net,total[(level,net)], attempts[(level,net)],correct[(level,net)]]) )
