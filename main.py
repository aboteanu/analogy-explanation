from model.sat_question_reader import SATReader
from model.levels_question_reader import LevelsReader
import answer_sse, answer_lra
from lra import lra

from model.conceptnet_api import conceptnet_api as cnapi
from model.wordnet_api import node_expansion as wnapi 
from model.cyc_api import node_expansion as cycapi

SEMANTIC_NETWORK =  'CYC'
#SEMANTIC_NETWORK =  'WN'
#SEMANTIC_NETWORK =  'CN'

SSE = True 
LRA = False 

def word_hit_miss( q ):
	# get only the input words for each semantic net
	for p in [q.question]+q.answers:
		a,b = map( str.strip, p.split(',') )
		res = cnapi( a )
		if len( res ) > 0:
			print ','.join( [ q.level, 'CN', '1', a ] )
		else:
			print ','.join( [ q.level, 'CN', '0', a ] )
		res = cnapi( b )
		if len( res ) > 0:
			print ','.join( [ q.level, 'CN', '1', b ] )
		else:
			print ','.join( [ q.level, 'CN', '0', b ] )
		res = wnapi( a )
		if len( res ) > 0:
			print ','.join( [ q.level, 'WN', '1', a ] )
		else:
			print ','.join( [ q.level, 'WN', '0', a ] )
		res = wnapi( b )
		if len( res ) > 0:
			print ','.join( [ q.level, 'WN', '1', b ] )
		else:
			print ','.join( [ q.level, 'WN', '0', b ] )
		res = cycapi( a )
		if len( res ) > 0:
			print ','.join( [ q.level, 'CYC', '1', a ] )
		else:
			print ','.join( [ q.level, 'CYC', '0', a ] )
		res = cycapi( b )
		if len( res ) > 0:
			print ','.join( [ q.level, 'CYC', '1', b ] )
		else:
			print ','.join( [ q.level, 'CYC', '0', b ] )

def build_lra_space():
	# build space
	wplist = set()
	for l in open( 'lra_analogy_file.txt', 'rt' ):
		wplist.add( tuple( l.strip().split(':') ) )
	wplist = list( wplist )
	lra.lra_build_space( wplist )

''' 
two word analogy
'''
if __name__=='__main__':
	#build_lra_space()
	#exit(0)

	print 'level,net,result,word'
	#print 'level,question_words,method,answer_score,answer,solution'
	for level in range(1,13) + ['sat']:
		if level == 'sat':
			qreader = SATReader()
		else:
			qreader = LevelsReader(level)
		 
		q_id = -1 
		for q in qreader:
			q_id+=1
			if not q:
				break
			q.level = str( level )
			q.id = q_id

			word_hit_miss( q )
			continue
			
			# answer question using SSE and different semantic nets
			if SSE:
				answer_sse.answer_question( q )
			if LRA:
				answer_lra.answer_question( q )
