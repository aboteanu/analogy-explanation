from lra import lra

def answer_question( question ):
        qwords = question.question.split(',') # question
        qans = list()
        n = len( question.answers )
        question_id = question.id
        for i in range(n):
                pair = question.answers[i].split(',')
                qans.append(pair)

	lra.lra_answer_question( question.level, question.solution, [qwords] + qans )
